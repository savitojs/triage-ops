# frozen_string_literal: true

require_relative File.expand_path('../lib/priority_mapping_helper.rb', __dir__)

Gitlab::Triage::Resource::Context.include PriorityMappingHelper
