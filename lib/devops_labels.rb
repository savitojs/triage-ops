# frozen_string_literal: true

require_relative 'www_gitlab_com'

class DevopsLabels
  CATEGORIES_PER_DEPARTMENT = {
    'Quality' => [
      'failure::broken-test',
      'failure::flaky-test',
      'failure::stale-test',
      'failure::test-environment',
      'master:broken',
      'master:needs-investigation',
      'QA'
    ].freeze,
    'Engineering Productivity' => [
      'dev-quality',
      'static analysis'
    ].freeze,
    'Technical Writing' => [
      'docs::feature',
      'docs::fix',
      'docs::housekeeping',
      'docs::improvement',
      'docs::new',
      'docs::revamp'
    ].freeze,
    'infrastructure' => ['infrastructure'].freeze
  }.freeze
  SECTION_LABEL_PREFIX = 'section::'
  STAGE_LABEL_PREFIX = 'devops::'
  GROUP_LABEL_PREFIX = 'group::'
  INFRASTRUCTURE_TEAM_LABEL_PREFIX = 'team::'
  CATEGORY_LABEL_PREFIX = 'Category:'
  DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE = ['Category:Navigation'].freeze

  def self.stages
    WwwGitLabCom.stages.keys
  end

  def self.groups
    WwwGitLabCom.groups.keys
  end

  def self.category_labels_per_group
    @category_labels_per_group ||=
      WwwGitLabCom.categories.each_with_object(Hash.new { |h,k| h[k] = [] }) do |(_, attrs), memo|
        group_key = attrs['group']
        memo[group_key] << attrs['label']
        memo[group_key].concat(attrs.fetch('feature_labels', []))
        memo[group_key].flatten!
      end
  end

  def self.category_labels_per_group_or_department
    @category_labels_per_group_or_department ||= category_labels_per_group.merge(CATEGORIES_PER_DEPARTMENT)
  end

  def self.category_labels
    @category_labels ||=
      category_labels_per_group.values.flatten +
        CATEGORIES_PER_DEPARTMENT.values.flatten
  end

  def self.department_labels
    @department_labels ||= CATEGORIES_PER_DEPARTMENT.keys
  end

  def self.department?(item)
    department_labels.include?(item)
  end

  def self.teams_per_user(username)
    gitlab_team_member = WwwGitLabCom.team_from_www[username]
    return [] if gitlab_team_member.nil?

    departments = gitlab_team_member['departments']
    return [] if departments.nil?

    teams = departments.select { |dept| dept.end_with?('Team') }
    sanitize_team_names(teams)
  end

  def self.sanitize_team_names(teams)
    teams.map do |team|
      team_name = team.delete_suffix(' Team').downcase
      team_name = drop_role(team_name)
      team_name = drop_stage(team_name)

      team_name.tr(' ', '_')
    end
  end
  private_class_method :sanitize_team_names

  def self.drop_role(team_name)
    %w[ux be fe pm].each do |role|
      return team_name.delete_suffix(" #{role}") if team_name.end_with?(role)
    end

    team_name
  end
  private_class_method :drop_role

  def self.drop_stage(team_name)
    stages.each do |stage|
      return team_name.delete_prefix("#{stage}:") if team_name.start_with?(stage)
    end

    team_name
  end
  private_class_method :drop_stage

  module Context
    extend self

    LabelsInferenceResult = Struct.new(
      :new_labels, :matching_labels, :silent_new_labels) do
      def any?
        new_labels.any?
      end

      def new_labels
        self[:new_labels] ? Array[self[:new_labels]].flatten.compact.uniq.sort : []
      end

      def matching_labels
        self[:matching_labels] ? Array[self[:matching_labels]].flatten.compact.uniq.sort : []
      end

      def silent_new_labels
        self[:silent_new_labels] ? Array[self[:silent_new_labels]].flatten.compact.uniq.sort : []
      end

      def comment
        return unless any?

        if silent_new_labels && (new_labels - silent_new_labels).empty?
          quick_actions
        else
          "#{explanation}\n#{quick_actions}"
        end
      end

      def quick_actions
        "/label #{new_labels_markdown}"
      end

      def merge(other_inference_result)
        self.class.merge(self, other_inference_result)
      end

      def self.merge(*label_inference_results)
        new_labels = label_inference_results.map(&:new_labels)
        matching_labels = label_inference_results.map(&:matching_labels)
        silent_new_labels = label_inference_results.map(&:silent_new_labels)

        self.new(new_labels, matching_labels, silent_new_labels)
      end

      def self.empty
        self.new
      end

      private

      def new_labels_markdown
        new_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def matching_labels_markdown
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def explanation
        "Setting label(s) #{new_labels_markdown} based on #{matching_labels_markdown}."
      end
    end

    MatchingGroup = Struct.new(:name, :matching_labels) do
      def in_stage?(stage)
        Context.stage_for_group(name) == stage
      end

      def markdown_labels
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end
    end

    MergeRequestAuthorGroupLabel = Struct.new(:author, :label) do
      def comment
        "#{explanation}\n#{quick_actions}"
      end

      private

      def explanation
        "Setting label #{markdown_label} based on `@#{author}`'s group."
      end

      def quick_actions
        "/label #{markdown_label}"
      end

      def markdown_label
        Context.markdown_label(label)
      end
    end

    def label_names
      @label_names ||= labels.map(&:name)
    end

    def find_scoped_label(labels_array, prefix)
      labels_array.find { |label| label.start_with?(prefix) && label.count(':') == 2 }
    end

    def current_section_label
      find_scoped_label(label_names, SECTION_LABEL_PREFIX)
    end

    def current_stage_label(matching_labels = label_names)
      find_scoped_label(matching_labels, STAGE_LABEL_PREFIX) ||
        (DevopsLabels.department_labels & matching_labels).first
    end

    def current_group_label(matching_labels = label_names)
      find_scoped_label(matching_labels, GROUP_LABEL_PREFIX)
    end

    def current_infrastructure_team_label
      find_scoped_label(label_names, INFRASTRUCTURE_TEAM_LABEL_PREFIX)
    end

    def current_department_label
      (DevopsLabels.department_labels & label_names).first
    end

    def current_category_labels
      DevopsLabels.category_labels & label_names
    end

    def current_stage_name(stage_label = current_stage_label)
      return unless stage_label

      stage_label.delete_prefix(DevopsLabels::STAGE_LABEL_PREFIX)
    end

    def current_group_name(group_label = current_group_label)
      return unless group_label

      group_label.delete_prefix(DevopsLabels::GROUP_LABEL_PREFIX).tr(' ', '_')
    end

    def all_category_labels_for_stage(stage_name)
      stage = WwwGitLabCom.stages[stage_name]
      return [] unless stage

      stage['groups'].flat_map do |group|
        all_category_labels_for_group(group)
      end
    end

    def all_category_labels_for_group(group)
      DevopsLabels.category_labels_per_group.fetch(group, [])
    end

    def section_for_stage(stage_name)
      WwwGitLabCom.stages.dig(stage_name, 'section')
    end

    def section_for_group(group_name)
      group = WwwGitLabCom.groups[group_name]
      return unless group

      group['section']
    end

    def has_department_label?
      !current_department_label.nil?
    end

    def has_section_label?
      !current_section_label.nil?
    end

    def has_stage_label?
      !current_stage_label.nil?
    end

    def has_group_label?
      !current_group_label.nil?
    end

    def has_any_kind_group_label?
      any_kind_group_prefix = GROUP_LABEL_PREFIX.chomp(':')

      label_names.any? { |name| name.start_with?(any_kind_group_prefix) }
    end

    def has_category_label?
      current_category_labels.any?
    end

    def has_category_label_for_current_stage?
      return false unless has_stage_label?

      !(all_category_labels_for_stage(current_stage_name) & label_names).empty?
    end

    def has_category_label_for_current_group?
      return false unless has_group_label?

      !(all_category_labels_for_group(current_group_name) & label_names).empty?
    end

    def can_infer_labels?
      return false if
        has_section_label? &&
        has_stage_label? &&
        has_group_label? &&
        has_category_label_for_current_group?

      has_stage_label? ||
        has_group_label? ||
        has_category_label?
    end

    def can_add_default_group?
      !has_group_label? &&
        current_infrastructure_team_label.nil? &&
        current_department_label.nil?
    end

    def can_add_default_group_and_stage?
      can_add_default_group? && !has_stage_label?
    end

    def can_infer_group_from_author?
      !!group_for_user(author)
    end

    def stage_has_a_single_group?(stage_name)
      stage = WwwGitLabCom.stages[stage_name]
      return false unless stage

      stage['groups'].one?
    end

    def group_part_of_stage?
      stage = WwwGitLabCom.stages[current_stage_name]
      return false unless stage

      stage['groups'].include?(current_group_name)
    end


    def first_group_for_stage(stage_name)
      stage = WwwGitLabCom.stages[stage_name]
      return unless stage

      stage['groups'].first
    end

    def stage_for_group(group_name)
      group = WwwGitLabCom.groups[group_name]
      return unless group

      group['stage']
    end

    def comment_for_intelligent_stage_and_group_labels_inference
      new_stage_and_group_labels = infer_new_labels

      inferred_labels = infer_section_label(new_stage_and_group_labels)

      inferred_labels.comment
    end

    def group_for_user(username)
      return if username.nil? || username.empty?

      teams = DevopsLabels.teams_per_user(username)
      matching_groups = teams.select do |team|
        DevopsLabels.groups.include?(team)
      end

      matching_groups.first if matching_groups.one?
    end

    def comment_for_mr_author_group_label
      group = group_for_user(author)

      return if group.nil?

      label = label_for(group: group)

      MergeRequestAuthorGroupLabel.new(author, label).comment
    end

    def markdown_label(label)
      %(~"#{label}")
    end

    private

    def infer_new_labels
      return LabelsInferenceResult.empty if has_stage_label? && has_group_label? && has_category_label_for_current_group?

      if has_stage_label?
        if has_group_label?
          infer_category_from_group
        elsif has_any_kind_group_label? # Including non-scoped group labels
          # TODO: Need a different way to infer from non-scoped group labels.
          # For now in this case we consider there's a group label so we don't
          # infer group label again.
          LabelsInferenceResult.empty
        else
          infer_group_from_category
        end
      else
        if has_group_label?
          infer_stage_and_category_from_group
        else
          infer_stage_and_group_from_category
        end
      end
    end

    def infer_group_from_stage
      return LabelsInferenceResult.empty unless stage_has_a_single_group?(current_stage_name)

      LabelsInferenceResult.new(label_for(group: first_group_for_stage(current_stage_name)), current_stage_label)
    end

    def infer_group_from_category
      matching_group = best_matching_group

      if matching_group && matching_group.in_stage?(current_stage_name)
        LabelsInferenceResult.new(label_for(group: matching_group.name), matching_group.matching_labels)
      elsif stage_matching_groups.none?
        infer_group_from_stage
      else
        LabelsInferenceResult.empty
      end
    end

    def infer_stage_and_group_from_category
      matching_group = best_matching_group
      return LabelsInferenceResult.empty unless matching_group

      stage_label = label_for(stage: stage_for_group(matching_group.name))
      group_label = label_for(group: matching_group.name)

      LabelsInferenceResult.new([stage_label, group_label], matching_group.matching_labels)
    end

    def infer_category_from_group
      category_labels = all_category_labels_for_group(current_group_name).filter do |category|
        category.start_with?(DevopsLabels::CATEGORY_LABEL_PREFIX)
      end

      if category_labels.one? && !current_category_labels.include?(category_labels.first) && !DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE.include?(category_labels.first)
        LabelsInferenceResult.new(category_labels.first, current_group_label)
      else
        LabelsInferenceResult.empty
      end
    end

    def infer_stage_from_group
      stage_label = label_for(stage: stage_for_group(current_group_name))

      LabelsInferenceResult.new(stage_label, current_group_label)
    end

    def infer_stage_and_category_from_group
      inferred_stage = infer_stage_from_group
      inferred_category = infer_category_from_group

      LabelsInferenceResult.merge(inferred_stage, inferred_category)
    end

    def infer_section_label(label_inference_result)
      # Already having a section label
      return label_inference_result if has_section_label?

      # Prefer the new labels over the current labels because new scoped
      # labels will replace the current scoped label
      eventual_labels = label_inference_result.new_labels | label_names

      group_label = current_group_label(eventual_labels)
      group_name = current_group_name(group_label)

      stage_label = current_stage_label(eventual_labels)
      stage_name = current_stage_name(stage_label)

      section_from_group = section_for_group(group_name)
      section_from_stage = section_for_stage(stage_name)

      # Nothing inferred
      return label_inference_result if
        section_from_group.nil? && section_from_stage.nil?

      # Ambiguous, different sections inferred from group and stage! Ignore
      # It may be a group under a different section works on a stage under
      # another section. See:
      # https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#naming-and-color-convention-1
      return label_inference_result if
        section_from_group && section_from_stage &&
        section_from_group != section_from_stage

      section_name, matching_label =
        if section_from_group
          [section_from_group, group_label]
        else
          [section_from_stage, stage_label]
        end

      inferred_section_label = label_for(section: section_name)

      label_inference_result.merge(
        LabelsInferenceResult.new(
          inferred_section_label,
          matching_label,
          inferred_section_label # we want this to be silent
        )
      )
    end

    def best_matching_group
      matching_groups = stage_matching_groups.any? ? stage_matching_groups : groups_from_current_category
      matching_labels_count = matching_groups.flat_map(&:matching_labels).size.to_f

      matching_groups.find do |matching_group|
        (matching_group.matching_labels.size / matching_labels_count) > 0.5
      end
    end

    def groups_from_current_category
      @groups_from_current_category ||=
        DevopsLabels.category_labels_per_group_or_department.each_with_object([]) do |(group, category_labels), matches|
          matching_category_labels = category_labels & label_names
          matches << MatchingGroup.new(group, matching_category_labels) if matching_category_labels.any?
        end
    end

    def stage_matching_groups
      @stage_matching_groups ||=
        groups_from_current_category.select do |matching_group|
          DevopsLabels.department?(matching_group.name) ||
            stage_for_group(matching_group.name) == current_stage_name
        end
    end

    def label_for(section: nil, stage: nil, group: nil)
      stage_or_group = stage || group
      return stage_or_group if DevopsLabels.department?(stage_or_group)

      if section
        WwwGitLabCom.sections[section]['label']
      elsif stage
        WwwGitLabCom.stages[stage]['label']
      elsif group
        WwwGitLabCom.groups[group]['label']
      end
    end
  end
end
