# frozen_string_literal: true

require 'yaml'
require 'httparty'

class ReportVerifier
  def initialize(policy_path)
    @policy_path = policy_path
  end

  def verify
    validate_policy!

    load_latest_reports.map do |report|
      report['title'].include?(report_title_without_interpolation)
    end.any?
  end

  private

  def validate_policy!
    raise ArgumentError.new('Policy file is not valid') unless report_project_path && report_title
  end

  def load_latest_reports
    response = HTTParty.get("https://gitlab.com/api/v4/projects/#{CGI.escape(report_project_path)}/issues?state=opened&labels=triage report&created_after=#{Date.today}")
    response.parsed_response
  end

  def policy
    @policy ||= YAML.load_file(@policy_path)
  end

  def report_project_path
    @report_project_path ||=
      policy.dig('resource_rules', 'issues', 'rules', 0, 'actions', 'summarize', 'destination')
  end

  def report_title
    @report_title ||=
      policy.dig('resource_rules', 'issues', 'rules', 0, 'actions', 'summarize', 'title')
  end

  def report_title_without_interpolation
    @report_title_without_interpolation ||=
      report_title.gsub(/\#{.+}/, '').chomp
  end
end
