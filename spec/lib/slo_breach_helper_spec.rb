# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/versioned_milestone'
require_relative '../../lib/slo_breach_helper'

RSpec.describe SloBreachHelper do
  let(:resource_klass) do
    Struct.new(:labels, :milestone) do
      include SloBreachHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }
  let(:milestone) { double() }

  subject { resource_klass.new(labels) }

  describe '#current_severity_label' do
    context 'with applied severity label' do
      let(:labels) { [label_klass.new(described_class::SEVERITY_LABELS[:s1])] }

      it 'returns correct string' do
        expect(subject.current_severity_label).to eq(described_class::SEVERITY_LABELS[:s1])
      end
    end

    context 'with no applied severity label' do
      it 'returns nil' do
        expect(subject.current_severity_label).to eq(nil)
      end
    end
  end

  context 'added labels with dates' do
    let(:slo_breach_helper) do
      Class.new do
        attr_reader :labels, :milestone

        include SloBreachHelper

        def initialize(labels, milestone)
          @labels = labels
          @milestone = milestone
        end

        def labels_with_details
        end
      end
    end

    let(:applied_severity) { described_class::SEVERITY_LABELS[:s1] }
    let(:labels) { [label_klass.new(applied_severity)] }
    let(:latest_severity_label) {
      double(name: applied_severity, added_at: label_added_at)
    }
    let(:label_added_at) { (described_class::SEVERITY_SLO_TARGETS[applied_severity] + 5).days.ago }

    subject { slo_breach_helper.new(labels, milestone) }

    before do
      allow(subject).to receive(:labels_with_details).and_return([latest_severity_label])
    end

    describe '#slo_breach_looming?' do
      before do
        allow_any_instance_of(VersionedMilestone).to receive(:current?).and_return(false)
      end

      context 'severity label applied and age makes slo breached' do
        it 'returns true' do
          expect(subject.slo_breach_looming?).to eq(true)
        end
      end

      context 'severity label applied and age makes slo warning' do
        let(:label_added_at) { (described_class::SEVERITY_SLO_TARGETS[applied_severity] - 5).days.ago }

        it 'returns true' do
          expect(subject.slo_breach_looming?).to eq(true)
        end
      end

      context 'severity label applied and age young' do
        let(:label_added_at) { 5.days.ago }

        it 'returns false' do
          expect(subject.slo_breach_looming?).to eq(false)
        end
      end

      context 'for current milestone' do
        before do
          allow_any_instance_of(VersionedMilestone).to receive(:current?).and_return(true)
        end

        it 'returns false' do
          expect(subject.slo_breach_looming?).to eq(false)
        end
      end
    end

    describe '#days_til_slo_breach' do
      around do |example|
        Timecop.freeze(2021, 01, 31) { example.run }
      end

      context 'severity label applied and age makes slo breached' do
        let(:label_added_at) { (described_class::SEVERITY_SLO_TARGETS[applied_severity] + 5).days.ago }

        it 'has correct value' do
          expected = "-5 days (2021-01-26)"

          expect(subject.days_til_slo_breach).to eq(expected)
        end
      end

      context 'severity label applied and age makes slo warning' do
        let(:label_added_at) { (described_class::SEVERITY_SLO_TARGETS[applied_severity] - 5).days.ago }

        it 'has correct value' do
          expected = "5 days (2021-02-05)"

          expect(subject.days_til_slo_breach).to eq(expected)
        end
      end

      context 'severity label applied and age young' do
        let(:label_added_at) { 5.days.ago }

        it 'has correct value' do
          expected = "25 days (2021-02-25)"

          expect(subject.days_til_slo_breach).to eq(expected)
        end
      end
    end
  end

  describe '#em_for_team' do
    context 'backend bug' do
      before do
        expect(subject).to receive(:backend_em_for_team).and_return('username')
      end

      it 'calls for backend em' do
        subject.em_for_team
      end
    end

    context 'frontend bug' do
      let(:labels) { [label_klass.new('frontend')] }
      before do
        expect(subject).to receive(:frontend_em_for_team).and_return('username')
      end

      it 'calls for frontend em' do
        subject.em_for_team
      end
    end
  end
end
